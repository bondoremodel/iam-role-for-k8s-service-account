output "iam_role_name" {
  value = local.iam_role_name
}

output "k8s_sa_annotation" {
  value = join(": ", local.k8s_sa_annotation)
}
