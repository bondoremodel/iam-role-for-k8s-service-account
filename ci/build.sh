#!/usr/bin/env sh

set -ev

OVERRIDES_FILE=ci-overrides.tf

echo '
provider "aws" {
  region     = "string"
  access_key = "string"
  secret_key = "string"
}
' > ${OVERRIDES_FILE}

terraform init
terraform validate -var-file ${CI_VARIABLES_FILE} .

rm -rf ci-*.tf* ${CI_FILES_DIR}
