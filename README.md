# aws-iam-role-for-service-account

Terraform module to create an AWS IAM role, the policy that goes with it and annotate a service account in order for related pods to assume the role.

Reference: https://docs.aws.amazon.com/en_pv/eks/latest/userguide/iam-roles-for-service-accounts.html
