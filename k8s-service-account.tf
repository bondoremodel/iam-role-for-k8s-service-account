locals {
  k8s_sa_annotation = ["eks.amazonaws.com/role-arn", local.iam_role_arn]
}

resource "null_resource" "annotate_service_account" {
  count = var.create && local.have_service_account ? 1 : 0

  provisioner "local-exec" {
    when    = create
    command = "kubectl annotate serviceaccount -n ${var.k8s_service_account_namespace} ${var.k8s_service_account_name} ${join("=", local.k8s_sa_annotation)} --overwrite --kubeconfig ${var.kubeconfig_file}"
  }

  triggers = {
    k8s_service_account_namespace = var.k8s_service_account_namespace
    k8s_service_account_name      = var.k8s_service_account_name
    iam_role                      = module.iam_assumable_role.this_iam_role_arn
    kubeconfig_file               = sha1(var.kubeconfig_file)
    external                      = var.external_trigger
    dependencies                  = sha1(join("", var.k8s_service_account_dependencies))
  }
}

resource "null_resource" "rollout_restart" {
  count = var.create ? length(var.k8s_fully_defined_resources) : 0

  provisioner "local-exec" {
    when    = create
    command = "kubectl rollout restart -n ${var.k8s_service_account_namespace} ${var.k8s_fully_defined_resources[count.index]} --kubeconfig ${var.kubeconfig_file}"
  }

  triggers = {
    k8s_service_account_namespace = var.k8s_service_account_namespace
    k8s_service_account_name      = var.k8s_service_account_name
    iam_role                      = module.iam_assumable_role.this_iam_role_arn
    kubeconfig_file               = sha1(var.kubeconfig_file)
    external                      = var.external_trigger
  }

  depends_on = [null_resource.annotate_service_account]
}
